/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deliverable2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.sf.marineapi.nmea.event.AbstractSentenceListener;
import net.sf.marineapi.nmea.io.SentenceReader;
import net.sf.marineapi.nmea.sentence.HDGSentence;
import net.sf.marineapi.nmea.sentence.MDASentence;
import net.sf.marineapi.nmea.sentence.MWVSentence;
import net.sf.marineapi.nmea.util.Position;

/**
 *
 * @author annag
 */
public class FXMLDocumentController implements Initializable {
    

    @FXML
    private VBox rootVBox;
    @FXML
    private Button button;
    @FXML
    private Button dnButtonA;
    @FXML
    private Button dnButtonB;
    @FXML
    private Button dnButtonC;
    @FXML
    private Label hdgLabel;
    @FXML
    private Label hdgL;
    @FXML
    private Label twdLabel;
    @FXML
    private Label twdL;
    @FXML
    private Label twsLabel;
    @FXML
    private Label twsL;
    @FXML
    private Label tempLabel;
    @FXML
    private Label tempL;
    @FXML
    private Label awaLabel;
    @FXML
    private Label awaL;
    @FXML
    private Label awsLabel;
    @FXML
    private Label awsL;
    @FXML
    private Label pitchLabel;
    @FXML
    private Label pitchL;
    @FXML
    private Label rollLabel;
    @FXML
    private Label rollL;
    @FXML
    private Label cogLabel;
    @FXML
    private Label cogL;
    @FXML
    private Label sogLabel;    
    @FXML
    private Label sogL;
    @FXML
    private Model model;
    @FXML
    private Label latLabel;
    @FXML
    private Label longLabel;
    @FXML
    private Label latL;
    @FXML
    private Label longL;
    @FXML
    private Button one;
    @FXML
    private Button two;
    @FXML
    private Button three;
    @FXML
    private ImageView dnImageViewA;
    @FXML
    private ImageView dnImageViewB;
    @FXML
    private ImageView dnImageViewC;
    @FXML private Slider slider;
    @FXML private Label sliderLabel;
    @FXML private TabPane tabpane;
    @FXML private LineChart<String, Double> chartTWD;
    @FXML private LineChart<String, Double> chartTWS;
    private ObservableList<Double> TWDdata;
    private ObservableList<Double> TWSdata;
    private boolean dayMode = true;
    //@FXML private ImageView imgDay;
    //@FXML private ImageView imgNight;
    //private AnchorPane ap;
    @FXML private GridPane helpGP1;
    @FXML private GridPane helpGP2;
    @FXML private Button left;
    @FXML private Button right;
    @FXML private ImageView leftImageView;
    @FXML private ImageView rightImageView;
    private boolean fst = true;
    private int page = 1;
    
    private void updateChart (LineChart chart, ObservableList list){
        XYChart.Series<String, Number> series = new XYChart.Series();
        
        if (list.size() == 601)
            list.remove(0);
        
        int bound = (list.size() > slider.getValue()*60) ? (int)(slider.getValue()*60) : list.size();
        
        int aux = (list.size() > slider.getValue()*60) ? list.size()-(int)slider.getValue()*60 : 0;
        
        for (int i = aux; i<list.size(); i++){
   
            series.getData().add(new XYChart.Data<>((int)(i/60) + ":" + (i%60), (double)list.get(i)));
        }
        chart.getData().clear();
        chart.getData().add(series);
        
    }
    
    @FXML
    private void sliderchange (){
        updateChart(chartTWD,TWDdata);
        updateChart(chartTWS,TWSdata);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TWDdata = FXCollections.observableArrayList();
        TWSdata = FXCollections.observableArrayList();
        
        chartTWD.setAnimated(false);
        chartTWS.setAnimated(false);
        
        tabpane.setTabMinWidth(90);
        tabpane.setTabMinHeight(90);
       
        
        twdL.setVisible(true);
        twdLabel.setVisible(true);
        twsL.setVisible(true);
        twsLabel.setVisible(true);
        awaL.setVisible(true);
        awaLabel.setVisible(true);
        awsL.setVisible(true);
        awsLabel.setVisible(true);
        
        hdgL.setVisible(false);
        hdgLabel.setVisible(false);
        tempL.setVisible(false);
        tempLabel.setVisible(false);
        pitchL.setVisible(false);
        pitchLabel.setVisible(false);
        rollL.setVisible(false);
        rollLabel.setVisible(false);
        
        latL.setVisible(false);
        latLabel.setVisible(false);
        longL.setVisible(false);
        longLabel.setVisible(false);
        cogL.setVisible(false);
        cogLabel.setVisible(false);
        sogL.setVisible(false);
        sogLabel.setVisible(false);
        
        one.getStyleClass().add("active");
        dnImageViewA.setImage(new Image("/images/sun.png"));
        dnImageViewB.setImage(new Image("/images/sun.png"));
        dnImageViewC.setImage(new Image("/images/sun.png"));
        
        
        leftImageView.setImage(new Image("/images/grey_left.png"));
        rightImageView.setImage(new Image("/images/day_right.png"));
        left.getStyleClass().add("day-button");
        right.getStyleClass().add("day-button");
        
        helpGP1.setVisible(true);
        helpGP2.setVisible(false);
    
        //imgDay.setImage(new Image("/images/day.jpg"));
        //imgNight.setImage(new Image("/images/night.jpg"));  
        
        model=Model.getInstance();
        
        //===============================================================
        //== For each property of the model, we add a listener. which updates
        //== the valur of its corresponding graphic node (using runLater, 
        //== because the code is executed in other secondary thread.
        
        model.HDGProperty().addListener((observable, oldValue, newValue) -> {
            String dat = String.valueOf(newValue) + "º";
                 Platform.runLater(() -> {
                hdgLabel.setText(dat);
            });
        });
        model.TWDProperty().addListener((observable, oldValue, newValue) -> {
            String dat = String.valueOf(newValue) + "º";
            double data = newValue.doubleValue();
            Platform.runLater(() -> {
                twdLabel.setText(dat);
                TWDdata.add(data);
                //System.out.print(TWDdata);
                updateChart(chartTWD, TWDdata);
            });
        });
        model.TWSProperty().addListener((observable, oldValue, newValue)-> {
            String dat = String.valueOf(newValue) + "Kn";
            double data = newValue.doubleValue();
            Platform.runLater(() -> {
                twsLabel.setText(dat);
                TWSdata.add(data);
                updateChart(chartTWS, TWSdata);
            });
        });
        model.TEMPProperty().addListener((observable, oldValue, newValue)-> {
            String dat = String.valueOf(newValue) + "ºC";
            Platform.runLater(() -> {
                tempLabel.setText(dat);
            });
        });
        model.AWAProperty().addListener((observable, oldValue, newValue) -> {
            String dat = String.valueOf(newValue) + "º";
            Platform.runLater(() -> {
                awaLabel.setText(dat);
            });
        });
        model.AWSProperty().addListener((observable, oldValue, newValue)-> {
            String dat = String.valueOf(newValue) + "Kn";
            Platform.runLater(() -> {
                awsLabel.setText(dat);
            });
        });
        model.GPSroperty().addListener((observable, oldValue, newValue)-> {
            Platform.runLater(() -> {                
                longLabel.setText(String.valueOf(Math.round(newValue.getLongitude() * 1000000.0) / 1000000.0) + " " + newValue.getLongitudeHemisphere());
                latLabel.setText(String.valueOf(Math.round(newValue.getLatitude() * 1000000.0) / 1000000.0) +  " " + newValue.getLatitudeHemisphere());             
            });
        });
        model.COGProperty().addListener((observable, oldValue, newValue) -> {
            String dat = String.valueOf(newValue) + "º";
                 Platform.runLater(() -> {
                cogLabel.setText(dat);
            });
        });
        model.SOGProperty().addListener((observable, oldValue, newValue) -> {
            String dat = String.valueOf(newValue) + "º";
                 Platform.runLater(() -> {
                sogLabel.setText(dat);
            });
        });
        model.ROLLProperty().addListener((observable, oldValue, newValue) -> {
            String dat = String.valueOf(newValue) + "º";
                 Platform.runLater(() -> {
                rollLabel.setText(dat);
            });
        });
        model.PITCHProperty().addListener((observable, oldValue, newValue) -> {
            String dat = String.valueOf(newValue) + "º";
                 Platform.runLater(() -> {
                pitchLabel.setText(dat);
            });
        });
        try {
        File ficheroNMEA = new File(System.getProperty("user.dir") + "/src/log/Jul_20_2017_1871339_0183.NMEA");
        if (ficheroNMEA != null) {

            model.addSentenceReader(ficheroNMEA);
        }
        } catch(Exception e){}
    }
    
    @FXML
    public void buttonClickOne(ActionEvent event) throws FileNotFoundException {
        twdL.setVisible(true);
        twdLabel.setVisible(true);
        twsL.setVisible(true);
        twsLabel.setVisible(true);
        awaL.setVisible(true);
        awaLabel.setVisible(true);
        awsL.setVisible(true);
        awsLabel.setVisible(true);

        hdgL.setVisible(false);
        hdgLabel.setVisible(false);
        tempL.setVisible(false);
        tempLabel.setVisible(false);
        pitchL.setVisible(false);
        pitchLabel.setVisible(false);
        rollL.setVisible(false);
        rollLabel.setVisible(false);

        latL.setVisible(false);
        latLabel.setVisible(false);
        longL.setVisible(false);
        longLabel.setVisible(false);
        cogL.setVisible(false);
        cogLabel.setVisible(false);
        sogL.setVisible(false);
        sogLabel.setVisible(false);
        
        if (page != 1) one.getStyleClass().add("active");
        two.getStyleClass().remove("active");
        three.getStyleClass().remove("active");
        page = 1;
    }
    
    public void buttonClickTwo(ActionEvent event) throws FileNotFoundException {
        twdL.setVisible(false);
        twdLabel.setVisible(false);
        twsL.setVisible(false);
        twsLabel.setVisible(false);
        awaL.setVisible(false);
        awaLabel.setVisible(false);
        awsL.setVisible(false);
        awsLabel.setVisible(false);

        hdgL.setVisible(true);
        hdgLabel.setVisible(true);
        tempL.setVisible(true);
        tempLabel.setVisible(true);
        pitchL.setVisible(true);
        pitchLabel.setVisible(true);
        rollL.setVisible(true);
        rollLabel.setVisible(true);
        

        latL.setVisible(false);
        latLabel.setVisible(false);
        longL.setVisible(false);
        longLabel.setVisible(false);
        cogL.setVisible(false);
        cogLabel.setVisible(false);
        sogL.setVisible(false);
        sogLabel.setVisible(false);
        
        one.getStyleClass().remove("active");
        if (page != 2) two.getStyleClass().add("active");
        three.getStyleClass().remove("active");
        page = 2;
    }
        
    public void buttonClickThree(ActionEvent event) throws FileNotFoundException {
        twdL.setVisible(false);
        twdLabel.setVisible(false);
        twsL.setVisible(false);
        twsLabel.setVisible(false);
        awaL.setVisible(false);
        awaLabel.setVisible(false);
        awsL.setVisible(false);
        awsLabel.setVisible(false);

        hdgL.setVisible(false);
        hdgLabel.setVisible(false);
        tempL.setVisible(false);
        tempLabel.setVisible(false);
        pitchL.setVisible(false);
        pitchLabel.setVisible(false);
        rollL.setVisible(false);
        rollLabel.setVisible(false);
        

        latL.setVisible(true);
        latLabel.setVisible(true);
        longL.setVisible(true);
        longLabel.setVisible(true);
        cogL.setVisible(true);
        cogLabel.setVisible(true);
        sogL.setVisible(true);
        sogLabel.setVisible(true);
        
        one.getStyleClass().remove("active");
        two.getStyleClass().remove("active");
        if (page != 3) three.getStyleClass().add("active");
        page = 3;
    }
    
    @FXML
    private void switchMode(ActionEvent event) {
        if (dayMode == true) {
            rootVBox.getStyleClass().remove("day");
            rootVBox.getStyleClass().add("night");
            dnButtonA.getStyleClass().remove("day-button");
            dnButtonA.getStyleClass().add("night-button");
            dnButtonB.getStyleClass().remove("day-button");
            dnButtonB.getStyleClass().add("night-button");
            dnButtonC.getStyleClass().remove("day-button");
            dnButtonC.getStyleClass().add("night-button");
            //dnbutton.setText("Switch to day mode");
            dnImageViewA.setImage(new Image("/images/moon.png"));
            dnImageViewB.setImage(new Image("/images/moon.png"));
            dnImageViewC.setImage(new Image("/images/moon.png"));
            
            if(fst != true) {
                leftImageView.setImage(new Image("/images/night_left.png"));
                rightImageView.setImage(new Image("/images/grey_right.png"));
            } else {
                leftImageView.setImage(new Image("/images/grey_left.png"));
                rightImageView.setImage(new Image("/images/night_right.png"));
            }
            
            left.getStyleClass().remove("day-button");
            left.getStyleClass().add("night-button");
            right.getStyleClass().remove("day-button");
            right.getStyleClass().add("night-button");
            
        } else {
            rootVBox.getStyleClass().remove("night");
            rootVBox.getStyleClass().add("day");
            dnButtonA.getStyleClass().remove("night-button");
            dnButtonA.getStyleClass().add("day-button");
            dnButtonB.getStyleClass().remove("night-button");
            dnButtonB.getStyleClass().add("day-button");
            dnButtonC.getStyleClass().remove("night-button");
            dnButtonC.getStyleClass().add("day-button");
            //dnbutton.setText("Switch to night mode");
            dnImageViewA.setImage(new Image("/images/sun.png"));
            dnImageViewB.setImage(new Image("/images/sun.png"));
            dnImageViewC.setImage(new Image("/images/sun.png"));
            
            if(fst != true) {
                leftImageView.setImage(new Image("/images/day_left.png"));
                rightImageView.setImage(new Image("/images/grey_right.png"));
            } else {
                leftImageView.setImage(new Image("/images/grey_left.png"));
                rightImageView.setImage(new Image("/images/day_right.png"));
            }
            left.getStyleClass().remove("night-button");
            left.getStyleClass().add("day-button");
            right.getStyleClass().remove("night-button");
            right.getStyleClass().add("day-button");
        }
        dayMode = !dayMode;
    }
    
    @FXML
    private void leftClickHelp (ActionEvent event) {
        if(fst != true && dayMode == true) {
            fst = true;
            leftImageView.setImage(new Image("/images/grey_left.png"));
            rightImageView.setImage(new Image("/images/day_right.png"));
            helpGP1.setVisible(true);
            helpGP2.setVisible(false);
        } else if(fst != true && dayMode == false) {
            fst = true;
            leftImageView.setImage(new Image("/images/grey_left.png"));
            rightImageView.setImage(new Image("/images/night_right.png"));
            helpGP1.setVisible(true);
            helpGP2.setVisible(false);
        }
    }
    
    @FXML
    private void rightClickHelp (ActionEvent event) {
        if(fst == true && dayMode == true) {
            fst = false;
            leftImageView.setImage(new Image("/images/day_left.png"));
            rightImageView.setImage(new Image("/images/grey_right.png"));
            helpGP2.setVisible(true);
            helpGP1.setVisible(false);
        } else if(fst == true && dayMode == false) {
            fst = false;
            leftImageView.setImage(new Image("/images/night_left.png"));
            rightImageView.setImage(new Image("/images/grey_right.png"));
            helpGP2.setVisible(true);
            helpGP1.setVisible(false);
        }
    }
    
}

